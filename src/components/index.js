import Header from './header.jsx';
import Showcases from './showcases.jsx';
import Contact from './contact.jsx';
import Footer from './footer.jsx';
import About from './about.jsx';
import Skills from './skills.jsx';
import Clients from './clients.jsx';

export { 
  Header,
  Showcases,
  Contact,
  Footer, 
  About, 
  Skills,
  Clients,
};